# JavaScript (ES6) Brunch  - app for searching recipes

Brunch  - built with JavaScript (ES6), CSS grid, flex boxes, SVG animation;
 
<p>
  <a href="https://js.oksana.tech" target="_blank">
    <img src="https://js.oksana.tech/brunch-screenshot.png">
    <br>
    Live Demo
  </a>
</p>

## Features
- Based on Udemy course from Jonas Schmedtmann, using his Free API http://forkify-api.herokuapp.com/

- Applied ES6 features:
  - New features of ES6: arrows, classes, const + let, template strings, modules etc. 
  - Asynchronous JavaScript: the event loop, promises, async/await, AJAX calls and APIs.
  - Local storage for Favourites Items.
  - Development workflow with NPM, Webpack, Babel and ES6 modules.

What I improved:

- Created new color scheme and designed new modern UI (using https://www.canva.com/colors/color-palette-generator/);

- Responsive design
  - CSS grid
  - Flex boxes
  - Media queries
  
  
- Animation
  - Created and animated SVG (bought on https://stock.adobe.com/ca/images/brunch-hand-written-lettering-logo-label-emblem-sign/175071747?prev_url=detail)
  - Added animation.css

- Fixed bugs with some recipes loading;
- Changed logic from fetching recipe by URL hash to Event Listeners;
- Added logic to draw default search results list, recipe image and ingredients based on default 'pizza' query;

## A Note on Functionality

This is a demo primarily aimed at explaining how to build a JavaScrips app, using ES6 features. 
There are a few things that can be done for improving functionality, for example:
 - Add remove all items from Shopping list and download the list;
 - Add remove Favourite item directly from Favourites list;
 - Add to local storage current application state (recipes and shopping list);
 

## Build Setup

**Requires Node.js 7+**

``` bash
# install dependencies
npm install

# serve in dev mode, with hot reload at localhost:8080
npm run start

# build for production
npm run build


## License

MIT
