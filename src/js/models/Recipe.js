import axios from 'axios';
import { baseApiUrl } from '../config';


export default class Recipe {
    constructor(id) {
        this.id = id;
    }

    async  getRecipe( ) {
        try {
            const res = await axios(`${baseApiUrl}/get?rId=${this.id}`);
            this.title = res.data.recipe.title;
            this.image = res.data.recipe.image_url;
            this.ingredients = res.data.recipe.ingredients;
            this.author = res.data.recipe.publisher;
            this.url = res.data.recipe.source_url;

        }catch(error) {
            console.log(`2 Error occurred ${error}`);
        }
    }

    calcTime(){
        const ingNumber = this.ingredients.length;
        const periods = Math.ceil(ingNumber / 3);
        this.time = periods * 15;
    }

    calcServings() {
        this.servings = 4;
    }

    parseIngredients() {
        const unitsLongNames = ['tablespoons', 'tablespoon', 'ounces', 'teaspoon', 'teaspoons',
        'cups', 'pounds', '+'];
        const unitsShortNames = ['tbsp', 'tbsp', 'oz', 'oz', 'tsp', 'cup', 'pound', 'add'];
        const units = [...unitsShortNames, 'kg', 'g', 'jars', 'packages'];

        const newIngredients = this.ingredients.map(el => {

            let ingredient = el.toLowerCase();

            unitsLongNames.forEach((unit, i) => {
                ingredient = ingredient.replace(unit, unitsShortNames[i]);
            });

            ingredient = ingredient.replace(/ *\([^]*]\) */g,"").replace(/\(|\)/g, "").replace(/\(|\)/g, "").toLowerCase();

            let ingArray = ingredient.split(' ');
            // ingArray = ingArray.filter(el => {
            //     return el ==! 'add';
            // });
            const unitIndex = ingArray.findIndex(el2 => units.includes(el2));


            let objIng;
            if (unitIndex > -1) {
                // There is a unit
                // Ex. 4 1/2 cups, arrCount is [4, 1/2] --> eval("4+1/2") --> 4.5
                // Ex. 4 cups, arrCount is [4]
                const arrCount = ingArray.slice(0, unitIndex);

                let count;
                if (arrCount.length === 1) {
                    count = eval(ingArray[0].replace('-', '+'));
                } else {
                    count = eval(ingArray.slice(0, unitIndex).join('+'));
                }

                objIng = {
                    count,
                    unit: ingArray[unitIndex],
                    ingredient: ingArray.slice(unitIndex + 1).join(' ')
                };

            } else if (parseInt(ingArray[0], 10)) {
                // There is NO unit, but 1st element is number
                objIng = {
                    count: parseInt(ingArray[0], 10),
                    unit: '',
                    ingredient: ingArray.slice(1).join(' ')
                }
            } else if (unitIndex === -1) {
                // There is NO unit and NO number in 1st position
                objIng = {
                    count: 1,
                    unit: '',
                    ingredient
                }
            }
             return objIng;


    });

    this.ingredients = newIngredients;
    }

    updateServings (type) {
        const newServings = type === 'dec' ? this.servings -1 : this.servings + 1;

        this.ingredients.forEach(ing => {
            ing.count *= (newServings / this.servings);

        });

        this.servings = newServings;
    }

}
