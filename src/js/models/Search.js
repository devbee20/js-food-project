import axios from "axios";
import {baseApiUrl} from '../config';

export default class Search {
    constructor(query) {
        this.query = query;
    }

    async  getResults() {
        try {
            const res = await axios(`${baseApiUrl}/search?q=${this.query}`);
            this.result = res.data.recipes;
        }catch(error) {
            this.result = {};
        }
    }

}