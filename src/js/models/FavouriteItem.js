export default class FavouriteItem {

    constructor() {
        this.favourites = [];
    }


    addToFavourite(id, title, author, image){
        const favourite = {id, title, author, image};
        this.favourites.push(favourite);

        this.persistData();
        return favourite;
    }


    deleteFavourite(id) {
        const itemIndex = this.favourites.findIndex(el => el.id === id );

        this.favourites.splice(itemIndex, 1);
        this.persistData();
    }


    isFavourite(id) {
        return this.favourites.findIndex(el => el.id === id) !== -1;
    }


    getFavouritesCount() {
        return this.favourites.length;
    }


    persistData() {
        localStorage.setItem('favouriteItems', JSON.stringify(this.favourites));
    }


    readStorage() {
        const storage = JSON.parse(localStorage.favouriteItems);
        if(storage) this.favourites = storage;
    }

}

