import { elements } from "./base";
import { limitRecipeTitle } from "./searchView";

export const toggleAddToFavButton = isFavourite => {
    const iconIdent = isFavourite ? 'icon-heart' : 'icon-heart-outlined';
    document.querySelector('.recipe__love use').setAttribute('href', `img/icons.svg#${iconIdent}`);
}


export const toggleFavItemsMenu = favItemsCount => {
    elements.favItemsMenu.style.visibility = favItemsCount > 0 ? 'visible': 'hidden';
}


export const renderFavItems = favItem => {
    const marckup = `
    <li>
        <a class="likes__link" href="#${favItem.id}">
            <figure class="likes__fig">
                <img src="${favItem.image}" alt="${favItem.title}">
            </figure>
            <div class="likes__data">
                <h4 class="likes__name">${limitRecipeTitle(favItem.title)}</h4>
                <p class="likes__author">${favItem.author}</p>
            </div>
        </a>
    </li>
    
    `;

    elements.favItemsList.insertAdjacentHTML('beforeend', marckup);
}


export const deleteFromFavList = id => {
    const el = document.querySelector(`.likes__link[href*="${id}"]`).parentElement;
    if(el) el.parentElement.removeChild(el);
}
