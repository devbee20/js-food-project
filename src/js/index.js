import Search from './models/Search';
import Recipe from './models/Recipe';
import ShoppingList from './models/ShoppingList';
import FavouriteItem from './models/FavouriteItem';
import * as searchView from './views/searchView';
import * as recipeView from './views/recipeView';
import * as shoppingListView from './views/shoppingListView';
import * as favouriteItemList from './views/favouriteItemList';
import {elements, renderLoader, clearLoader} from './views/base';

/** Global state of the app
 * - Searah object
 * - current recipe object
 * - shopping list object
 * - liked recipes
 *
 * */
const state = {};

/**
 * Control first page load and set default query
 * @returns {Promise<void>}
 */
const controlDefaultState = async () => {

    const query = 'pizza';

    if (query) {
        state.search = new Search(query);

        renderLoader(elements.searchRes);

        try {
            await state.search.getResults();
            clearLoader();
            searchView.renderResults(state.search.result);
            if (state.search.result.length) {
                const firstElement = state.search.result[0];
                const firstElId = firstElement.recipe_id;
                controlRecipe(firstElId);
            }

        } catch (error) {
            console.log(error);
        }

    }
}

/**
 * Search controller
 * @returns {Promise<void>}
 */
const controlSearch = async () => {

    const query = searchView.getInput();

    if (query) {
        state.search = new Search(query);
        searchView.clearInput();
        searchView.clearSearchList();
        renderLoader(elements.searchRes);

        try {
            await state.search.getResults();
            clearLoader();
            searchView.renderResults(state.search.result);
            const firstElement = state.search.result[0];
            const firstElId = firstElement.recipe_id;
            controlRecipe(firstElId);
        } catch (error) {
            console.log(error);
        }

    }
}


elements.searchForm.addEventListener('submit', e => {
    e.preventDefault();
    controlSearch();
});


elements.searchListPagination.addEventListener('click', element => {
        const btn = element.target.closest('.btn-inline');
        if (btn) {
            const goToPage = parseInt(btn.dataset.goto, 10);
            searchView.clearSearchList();
            searchView.renderResults(state.search.result, goToPage);
        }
    }
)


elements.searchList.addEventListener('click', element => {
        element.preventDefault();
        const link = element.target.closest('.results__link');
        const currentId = link.getAttribute("href").replace('#', '');
        controlRecipe(currentId);
    }
)


elements.favItemsList.addEventListener('click', element => {
        element.preventDefault();
        const link = element.target.closest('.likes__link');
        const currentId = link.getAttribute("href").replace('#', '');
        controlRecipe(currentId);
    }
)


/**
 * Recipe controller
 * @returns {Promise<void>}
 */
const controlRecipe = async (id) => {
    if (id) {

        recipeView.clearRecipe();
        renderLoader(elements.recipe);

        if (state.search) searchView.highlightSelected(id);

        state.recipe = new Recipe(id);

       // try {
            await state.recipe.getRecipe();
            state.recipe.parseIngredients();
            state.recipe.calcTime();
            state.recipe.calcServings();

            clearLoader();
            recipeView.renderRecipe(
                state.recipe,
                state.favouriteItem.isFavourite(id)
            );
        // } catch (error) {
        //     console.log(`1 An Error occurs during processing recipe ${error}`);
        // }


    }
}


/**
 * Shopping List controller
 */
const controlShoppingList = () => {

    if (!state.shoppingList) state.shoppingList = new ShoppingList();

    state.recipe.ingredients.forEach(el => {
            const item = state.shoppingList.addItem(el.count, el.unit, el.ingredient);
            shoppingListView.renderItem(item);
        }
    );
};


//Update and delete shopping list items
elements.shoppingList.addEventListener('click', e => {
    const id = e.target.closest('.shopping__item').dataset.itemid;

    if (e.target.matches('.shopping__delete, .shopping__delete *')) {
        state.shoppingList.deleteItem(id);
        shoppingListView.deleteItem(id);
    } else if (e.target.matches('.shopping__count-value')) {
        const val = parseFloat(e.target.value);
        state.shoppingList.updateCount(id, val);
        console.log(state);
    }
});


// Handle show Recipe
//['hashchange', 'load'].forEach(event => window.addEventListener(event, controlRecipe));

// Handle default state

window.addEventListener('load', () => {

    controlDefaultState();

})


/**
 * Favourite Items controller
 */

state.favouriteItem = new FavouriteItem();
const controlFavouriteItems = () => {
    if (!state.favouriteItem) state.favouriteItem = new FavouriteItem();
    const currentId = state.recipe.id;

    if (!state.favouriteItem.isFavourite(currentId)) {
        const newFavourite = state.favouriteItem.addToFavourite(
            currentId,
            state.recipe.title,
            state.recipe.author,
            state.recipe.image
        );

        console.log(newFavourite);
        favouriteItemList.toggleAddToFavButton(true);
        favouriteItemList.renderFavItems(newFavourite);
    } else {
        state.favouriteItem.deleteFavourite(currentId);
        favouriteItemList.toggleAddToFavButton(false);
        favouriteItemList.deleteFromFavList(currentId);
    }

    favouriteItemList.toggleFavItemsMenu(state.favouriteItem.getFavouritesCount());
}


// Restore Favourite items
window.addEventListener('load', () => {
    state.favouriteItem.readStorage();
    favouriteItemList.toggleFavItemsMenu(state.favouriteItem.getFavouritesCount());
    state.favouriteItem.favourites.forEach(el => favouriteItemList.renderFavItems(el));
})


// Handle Recipe button clicks
elements.recipe.addEventListener('click', e => {
    if (e.target.matches('.btn-decrease, .btn-decrease *')) {
        console.log('clicked btn-decrease');
        if (state.recipe.servings > 1) {
            state.recipe.updateServings('dec');
            recipeView.updateServingsIngredients(state.recipe);
        }
    } else if (e.target.matches('.btn-increase, .btn-increase *')) {
        console.log('clicked btn-increase');
        state.recipe.updateServings('inc');
        recipeView.updateServingsIngredients(state.recipe);
    } else if (e.target.matches('.recipe__btn--add, .recipe__btn--add *')) {
        controlShoppingList();
    } else if (e.target.matches('.recipe__love, .recipe__love *')) {
        controlFavouriteItems();
    }
});

